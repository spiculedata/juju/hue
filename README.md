# Overview

HUE is an analytical workbench for lightweight Business Intelligence (BI). 
It provides an overlay to Hadoop to make finding and querying data quick 
and easy. HUE stands for Hadoop User Experience. Hadoop can be thought of as a 
set of open source programs and procedures (essentially, they are free for
anyone to use or modify, with a few exceptions) which anyone can use as the 
"backbone" of their big data operations.

# Features of Hue
1. Allows easy finding of data. Users can browse and search for datasets within an organisation from within the Hue workbench/dashboard. 
2. Querying the data. Advanced users can leverage the smart query editor while less technical users have sets of discoverable queries and presentations available which can be modified for their individual use.
3. Getting results from the data. Results from the queries can very easily be charted and pasted into whatever application or report required. 


# Usage

To deploy this charm simply run:
    
    juju deploy cs:~spiculecharms/bundle/anssr-hadoop
    juju deploy cs:~spicule/hue
    juju add-relation hue hadoop-plugin
    juju expose hue



## MySQL Connectivity
To add MySQL connectivity simply run:

    juju deploy cs:mysql
    juju add-relation hue mysql

## HDFS connectivity

If you are running a Hadoop setup, you can also test the HDFS connectivity.

    juju add-relation hue namenode
    
This will add a datasource entry for your Hadoop namenode. You can then query CSV/JSON/Parquet files.

## Scale out Usage


## Known Limitations and Issues
Currently, if Hue is run without a database software relation such as MySQL of Postgres then a bunch of error messages appear upon opening Hue from the Juju GUI. 


# Configuration



# Contact Information

  - Contact: tom@analytical-labs.com

## Hue

  - http://gethue.com/
  - https://github.com/cloudera/hue

## Charm Support
  If you require commercial support for this charm or Hue, please contact us and we'd be happy to help.
  Email us at info@spicule.co.uk and we can arrange a call to discuss your requirements.