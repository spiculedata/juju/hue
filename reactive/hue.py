from charms.reactive import when, when_not, set_flag, hook, when_all, when_any, endpoint_from_flag
from charms.templating.jinja2 import render
from charmhelpers.core.hookenv import resource_get, status_set, log, open_port
from charms.reactive.helpers import data_changed
from subprocess import check_call
from charmhelpers.core.host import service_start, service_restart
from charms.apt import queue_install
import pickle

@hook('install')
def install():
    queue_install(['mysql-client', 'expect', 'libffi-dev', 'libgmp3-dev', 'libkrb5-dev', 'libldap2-dev', 'libmysqlclient-dev', 'libpq-dev', 'libsasl2-dev', 'libsasl2-modules-gssapi-mit', 'libsqlite3-dev', 'libssl-dev', 'libtidy-0.99-0', 'libxml2-dev', 'python-dev', 'python-setuptools'])

@when('apt.installed.expect')
@when_not('hue.installed')
def install_hue():
    #Get the path to the resource
    archive = resource_get("hue")

    #Create the Hue User
    cmd = ['useradd', 'hue']
    check_call(cmd)

    #Unzip the software
    cmd = ['tar', 'xfz', archive, '-C', '/opt']
    check_call(cmd)

    #Change the owner
    cmd = ['chown', '-R', 'hue:hue', '/opt/hue']
    check_call(cmd)

    #Create the empty config
    init_config_dict()

    open_port('8000')
    render('hue', '/etc/init.d/hue')
    cmd = ['chmod', '+x', '/etc/init.d/hue']
    check_call(cmd)
    render('create_dir.sh', '/root/create_dir.sh')

    #cmd = ['expect', '/root/createsu']
    #check_call(cmd)

    status_set('waiting', 'Installed, awaiting hadoop.')
    set_flag('hue.installed')


@when_not('mysql-backend.available')
def wait_mysql():
    status_set('waiting', 'Installed, awaiting mysql.')


@when_all('mysql-backend.available', 'hadoop.ready', 'hue.installed')
@when_not('hue.configured')
def configure_hadoop(mysql, hadoop):
    nn = hadoop.namenodes()
    rm = hadoop.resourcemanagers()
    cmd = ['sh', '/root/create_dir.sh']
    check_call(cmd)

    #Check for changes to the data
    if data_changed('available.hadoop_namenodes', nn) or data_changed('available.hadoop_resourcemanagers', rm):
        context = {
        'hadoop_namenode_hostname' : hadoop.namenodes()[0],
        'hadoop_namenode_port': '8020',
        'hadoop_resourcemanager_hostname': hadoop.resourcemanagers()[0],
        'hadoop_resourcemanager_port': '8032',
        'hadoop_resourcemanager_api_url': 'http://'+hadoop.resourcemanagers()[0]+':8088',
        'hadoop_history_server_api_url': 'http://'+hadoop.resourcemanagers()[0]+':19888',
        'mysql_backend_host': mysql.host(),
        'mysql_backend_user': mysql.user(),
        'mysql_backend_password': mysql.password(),
        'database_backend_name': mysql.database()
        }
        #Save the config
        set_config_dict(context)
    cmd = 'mysql -u{} -p{} -h{} {} < ./database/hue.sql'.format(mysql.user(), mysql.password(), mysql.host(), mysql.database())
    check_call(cmd, shell=True)
    service_start('hue')
    status_set('active', 'Hue running.')
    set_flag('hue.configured')



@when_all('hue.configured', 'hive.ready')
def configure_hive(hive):
    log("configuring hive")
    if data_changed('available.hive_hostname', hive.get_private_ip()) or data_changed('available.hive_port', hive.get_port()):
        log("configuring hive data changed")
        context ={
        'hive_hostname' : hive.get_private_ip(),
        'hive_port' : hive.get_port(),
        'hiveserver2_enabled': ''
        }
        set_config_dict(context)
        restart_hue()

@when_all('hue.configured', 'hbase.ready')
def configure(hbase):
    n = ''
    p = ''
    for unit in hbase.hbase_servers():
        n += unit['host']+","
        p = '9090'
    if data_changed('available.hbase_servers', hbase.hbase_servers()):
        context = {
        'hbase_cluster': '(Cluster|'+n+':'+p+')'
        }
        set_config_dict(context)
        restart_hue()


def init_config_dict():
    context = {
        'hadoop_namenode_hostname':'',
        'hadoop_namenode_port':'',
        'hadoop_webhdfs_port':'50070',
        'hadoop_resourcemanager_hostname':'',
        'hadoop_resourcemanager_port': '',
        'hadoop_resourcemanager_api_url':'',
        'hadoop_history_server_api_url': '',
        'hive_hostname': '',
        'hive_port': '',
        'hbase_cluster': '',
        'mysql_hostname': '',
        'mysql_name': '',
        'mysql_port': '',
        'mysql_user': '',
        'mysql_password': '',
        'pgsql_hostname': '',
        'pgsql_port': '',
        'pgsql_name': '',
        'pgsql_user': '',
        'pgsql_password': '',
        'mysql_enabled': '#',
        'pgsql_enabled': '#',
        'hiveserver2_enabled': '#',
        'livy_enabled': '#',
        'livy_ip': '',
        'livy_port': '',
        'livy_batch_enabled': '#',
        'rdbms_enabled': '#',
        'solr_enabled': '#',
        'oozie_enabled': '#',
        'oozie_hostname': '',
        'oozie_port': '',
        'mysql_backend_host': '',
        'mysql_backend_user': '',
        'mysql_backend_password': '',
        'database_backend_name': ''
    }
    pickle.dump( context, open( "save.p", "wb" ) )


def get_config_dict():
    context = pickle.load( open( "save.p", "rb" ) )
    return context


def set_config_dict(context):
    old = get_config_dict()
    context = merge_two_dicts(old, context)
    pickle.dump( context, open( "save.p", "wb" ) )
    render('hue.ini', '/opt/hue/desktop/conf/hue.ini', context)
    render('pseudo-distributed.ini', '/opt/hue/desktop/conf/pseudo-distributed.ini')
    #added

def merge_two_dicts(x, y):
    z = x.copy()
    z.update(y)
    return z

@when('hue.configured')
@when('mysql.available')
def configure_mysql(mysql):
    """
        Configure MySQL when a relation is added.
    """
    log("configuring mysql server" + mysql.host())
    if data_changed('available.mysql_hostname', mysql.host()) or data_changed('available.mysql_port', mysql.port()):
        context = {
            'mysql_hostname': mysql.host(),
            'mysql_port': mysql.port(),
            'mysql_name': mysql.database(),
            'mysql_user': mysql.user(),
            'mysql_password': mysql.password(),
            'mysql_enabled': ''
        }
        set_config_dict(context)
        restart_hue()

@when('hue.configured')
@when('pgsql.master.available')
def configure_pgsql(psql):
    """
        Configure Postgres when a relation is added.
    """
    log("configuring psql server" + psql.master.host+psql.master.port)
    cmd = ['/opt/hue/build/env/bin/pip', 'install', 'psycopg2']
    check_call(cmd)
    if data_changed('available.pgsql_hostname', psql.master.host) or data_changed('available.pgsql_port', psql.master.port):
        context = {
            'pgsql_hostname': psql.master.host,
            'pgsql_port': psql.master.port,
            'pgsql_name': psql.master.dbname,
            'pgsql_user': psql.master.user,
            'pgsql_password': psql.master.password,
            'pgsql_enabled': ''
        }
        set_config_dict(context)
        restart_hue()

@when('hue.configured')
@when('livy.available')
def set_livy(jdbc):
    if data_changed('available.livy_ip', jdbc.ip()) or data_changed('available.livy_port', jdbc.port()):
        context = {
            'livy_enabled': '',
            'livy_ip': jdbc.ip(),
            'livy_port': jdbc.port()

        }
        set_config_dict(context)
        restart_hue()

@when('hue.configured')
@when('oozie.available')
def configure_oozie(oozie):
    if data_changed('available.oozie_ip', oozie.ip()) or data_changed('available.oozie_port', oozie.port()):
        context = {
            'oozie_hostname': oozie.ip(),
            'oozie_port': oozie.port(),
            'oozie_enabled': '',

        }
        set_config_dict(context)
        restart_hue()


def restart_hue():
    service_restart('hue')